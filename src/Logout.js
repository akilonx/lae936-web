import React, { useState, useEffect } from 'react'
import Cookies from 'js-cookie'
import { Redirect } from 'react-router-dom'

export default function Logout(props) {
  Cookies.remove('jwt')
  Cookies.remove('signedin')
  Cookies.remove('token')
  Cookies.remove('FirstName')
  Cookies.remove('LastName')
  Cookies.remove('Department')
  Cookies.remove('StaffID')
  Cookies.remove('signedin')
  Cookies.remove('token')
  props.signedIn(false)

  return <Redirect to="/" />
}
