import React from 'react'
import Grid from '@material-ui/core/Grid'

import { makeStyles, withStyles, useTheme } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {},
  icon: { paddingLeft: 3 },
  subtitle: { fontSize: '1rem', lineHeight: 1 }
}))

export default function Subtitle(props) {
  const classes = useStyles()

  return (
    <React.Fragment>
      <Grid container direction="row" alignItems="center">
        <Grid item>{props.icon}</Grid>
        <Grid item>
          <div className={classes.icon}>{props.title}</div>
        </Grid>
      </Grid>
      <Grid container direction="row" alignItems="center">
        <Grid item className={classes.subtitle}>
          {props.subtitle}
        </Grid>
      </Grid>
    </React.Fragment>
  )
}
