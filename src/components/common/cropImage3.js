const createImage = (url) =>
  new Promise((resolve, reject) => {
    const image = new Image()
    image.addEventListener('load', () => resolve(image))
    image.addEventListener('error', (error) => reject(error))
    image.setAttribute('crossOrigin', 'anonymous') // needed to avoid cross-origin issues on CodeSandbox
    image.src = url
  })

function getRadianAngle(degreeValue) {
  return (degreeValue * Math.PI) / 180
}

/**
 * This function was adapted from the one in the ReadMe of https://github.com/DominicTobias/react-image-crop
 * @param {File} image - Image File url
 * @param {Object} pixelCrop - pixelCrop Object provided by react-easy-crop
 * @param {number} rotation - optional rotation parameter
 */

export default async function getCroppedImg3(imageSrc) {
  const image = await createImage(imageSrc)
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')
  // draw rotated image and store data.
  ctx.drawImage(image, image.width, image.height)

  const data = ctx.getImageData(0, 0, image.width, image.height)

  canvas.width = image.width
  canvas.height = image.height

  ctx.putImageData(data, image.width, image.height)
  // set canvas width to final desired crop size - this will clear existing context
  /* canvas.width = image.width
  canvas.height = image.height */

  // As Base64 string
  // return canvas.toDataURL('image/jpeg');

  // As a blob
  /* return new Promise((resolve) => {
    canvas.toBlob((file) => {
      resolve(file)
    }, 'image/jpeg')
  }) */
  return new Promise((resolve) => {
    canvas.toBlob((file) => {
      resolve(URL.createObjectURL(file))
    }, 'image/jpeg')
  })
}
