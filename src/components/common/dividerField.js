import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: 'transparent'
  }
}))

export default function DividerField(props) {
  const classes = useStyles()
  return <Divider {...props} classes={{ root: classes.root }} />
}
