import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  paper: {
    marginTop: theme.spacing(3),
    width: '100%',
    overflowX: 'auto',
    marginBottom: theme.spacing(2),
    background: theme.palette.primary.paper
  },
  table: {
    minWidth: 650
  }
}))

export default function SimpleTable(props) {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            {props.tableHeader.map((header, index) => (
              <TableCell key={index}>{header.title}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.tableData.map(row => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.ConsignmentNo}
              </TableCell>
              <TableCell>{row.Destination}</TableCell>
              <TableCell>{row.Pieces}</TableCell>
              <TableCell>{row.Weight}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  )
}
