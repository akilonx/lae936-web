import React, { Component } from "react";

class MenuButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open ? this.props.open : false,
      color: this.props.open ? "white" : "black"
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open !== this.state.open) {
      console.log("props color", this.props.color);
      this.setState({ open: nextProps.open, color: nextProps.open ? "white" : "black" });
    }
  }

  handleClick() {
    console.log("here");
    this.setState({ open: !this.state.open });
  }

  render() {
    const styles = {
      container: {
        height: "42px",
        width: "42px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer",
        padding: "4px",
        position: "fixed",
        right: "40px",
        top: "15px",
        zIndex: "99"
      },
      line: {
        height: "4px",
        width: "30px",
        background: this.state.color,
        transition: "all 0.2s ease"
      },
      lineTop: {
        transform: this.state.open ? "rotate(45deg)" : "none",
        transformOrigin: "top left",
        marginBottom: "5px"
      },
      lineMiddle: {
        opacity: this.state.open ? 0 : 1,
        transform: this.state.open ? "translateX(-16px)" : "none"
      },
      lineBottom: {
        transform: this.state.open ? "translateX(-3px) translateY(3px) rotate(-45deg)" : "none",
        transformOrigin: "top left",
        marginTop: "5px"
      }
    };
    return (
      <div
        style={styles.container}
        onClick={
          this.props.onClick
            ? this.props.onClick
            : () => {
                this.handleClick();
              }
        }
      >
        <div className="burger-icon" style={{ ...styles.line, ...styles.lineTop }} />
        <div className="burger-icon" style={{ ...styles.line, ...styles.lineMiddle }} />
        <div className="burger-icon" style={{ ...styles.line, ...styles.lineBottom }} />
      </div>
    );
  }
}

export default MenuButton;
