import React from 'react'
//
import useDemoConfig from './useDemoConfig'
import { Chart } from 'react-charts'

export default function Line() {
  const { data, randomizeData } = useDemoConfig({
    series: 1,
    dataType: 'ordinal',
    useR: true,
  })

  const series = React.useMemo(
    () => ({
      showPoints: true,
      type: 'line',
    }),
    []
  )

  const axes = React.useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { type: 'linear', position: 'left', max: 5 },
    ],
    []
  )

  return (
    <React.Fragment>
      <div style={{ height: 200, overflow: 'hidden' }}>
        <Chart data={data} series={series} axes={axes} tooltip />
      </div>
    </React.Fragment>
  )
}
