import { gql } from 'apollo-boost'

export const GET_ORDERBYDRIVER = gql`
  {
    orderbydriver {
      id
      OrderNo
      CustomerCode
      CustomerName
      Address1
      Address2
      ZoneID
      NoOfCarton
      Remarks
      Status
      CreatedOn
    }
  }
`

export const GET_STATUSLIST = gql`
  {
    statuslist {
      id
      StatusName
      Ordering
      StatusCode
    }
  }
`

export const GET_ZONES = gql`
  {
    zones {
      id
      ZoneID
      DriverID
      DriverName
      DriverNo
      ContactNo
      ZoneTitle
      ZoneDesc
      ZoneArea
      Orders {
        id
        OrderNo
        CustomerCode
        CustomerName
        Address1
        Address2
        ZoneID
        NoOfCarton
        Remarks
        Status
        CreatedOn
      }
    }
  }
`

export const REMOVE_ZONE = gql`
  mutation RemoveZoneDetail($id: ID!) {
    removezonedetail(id: $id)
  }
`

export const UPDATE_ZONE = gql`
  mutation UpdateZoneDetail(
    $id: ID!
    $DriverID: ID
    $ZoneTitle: String
    $ZoneDesc: String
    $ZoneArea: String
  ) {
    updatezonedetail(
      id: $id
      DriverID: $DriverID
      ZoneTitle: $ZoneTitle
      ZoneDesc: $ZoneDesc
      ZoneArea: $ZoneArea
    ) {
      id
      DriverID
      ZoneTitle
      ZoneDesc
      ZoneArea
    }
  }
`

export const CREATE_ZONE = gql`
  mutation CreateZoneDetail(
    $DriverID: ID
    $ZoneTitle: String
    $ZoneDesc: String
    $ZoneArea: String
  ) {
    createzonedetail(
      DriverID: $DriverID
      ZoneTitle: $ZoneTitle
      ZoneDesc: $ZoneDesc
      ZoneArea: $ZoneArea
    ) {
      id
      DriverID
      ZoneTitle
      ZoneDesc
      ZoneArea
    }
  }
`
