import { gql } from 'apollo-boost'

export const GET_PORT_SELECT = gql`
  {
    airports {
      id
      PortName
      PortID
      Sequence
    }
  }
`
