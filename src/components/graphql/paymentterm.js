import { gql } from 'apollo-boost'

export const GET_PAYMENTTERMS = gql`
  {
    paymentterms {
      id
      PaymentTermCode
      PaymentTermDesc
      PaymentTermDays
    }
  }
`
