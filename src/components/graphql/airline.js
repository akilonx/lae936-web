import { gql } from 'apollo-boost'

export const GET_AIRLINE_SELECT = gql`
  {
    airlines {
      id
      AirlineName
      AirlineID
    }
  }
`
