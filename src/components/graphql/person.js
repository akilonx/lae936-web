import { gql } from 'apollo-boost'

export const GET_PERSONS = gql`
  query Persons($ClientID: ID!) {
    persons(ClientID: $ClientID) {
      id
      ClientID
      FirstName
      LastName
      Email
      Mobile
      Phone
      PreAlert
      Active
      Username
      Password
      PersonInCharge
    }
  }
`
export const CREATE_PERSON = gql`
  mutation CreatePerson(
    $ClientID: ID!
    $FirstName: String
    $LastName: String
    $Email: String
    $Mobile: String
    $Phone: String
    $PreAlert: String
    $PersonInCharge: String
  ) {
    createperson(
      ClientID: $ClientID
      FirstName: $FirstName
      LastName: $LastName
      Email: $Email
      Mobile: $Mobile
      Phone: $Phone
      PreAlert: $PreAlert
      PersonInCharge: $PersonInCharge
    ) {
      id
      ClientID
      FirstName
      LastName
      Email
      Mobile
      Phone
      PreAlert
      Active
      Username
      Password
      PersonInCharge
    }
  }
`
export const UPDATE_PERSON = gql`
  mutation UpdatePerson(
    $id: ID!
    $FirstName: String
    $LastName: String
    $Email: String
    $Mobile: String
    $Phone: String
    $PreAlert: String
    $PersonInCharge: String
  ) {
    updateperson(
      id: $id
      FirstName: $FirstName
      LastName: $LastName
      Email: $Email
      Mobile: $Mobile
      Phone: $Phone
      PreAlert: $PreAlert
      PersonInCharge: $PersonInCharge
    ) {
      id
      ClientID
      FirstName
      LastName
      Email
      Mobile
      Phone
      PreAlert
      Active
      Username
      Password
      PersonInCharge
    }
  }
`

export const DELETE_PERSON = gql`
  mutation DeletePerson($id: ID!) {
    deleteperson(id: $id)
  }
`
