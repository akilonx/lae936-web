import { gql } from 'apollo-boost'

export const GET_AGENT_SELECT = gql`
  {
    agents {
      id
      CustomerCode
      CustomerName
      CoLoaderCode
    }
  }
`
