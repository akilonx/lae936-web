import { gql } from 'apollo-boost'

export const GET_CUSTOMERTYPE = gql`
  {
    customertypes {
      id
      CompanyTypeCode
      CompanyTypeName
    }
  }
`
