import { gql } from 'apollo-boost'

export const GET_QUOTATIONS = gql`
  query Quotations($CustomerCode: String) {
    quotations(CustomerCode: $CustomerCode) {
      id
      QuotationNo
      CustomerCode
      CurrencyCode
      AgreementDate
      EffectiveDate
      ExpiryDate
      Remark
      Active
      AirlineCode
    }
  }
`

export const GET_QUOTATION_DETAILS = gql`
  query QuotationDetail($QuotationNo: String!) {
    quotationdetails(QuotationNo: $QuotationNo) {
      id
      QuotationNo
      PortID
      ItemCode
      UnitPrice
      ItemDesc
      CustomerCode
      ItemType
    }
  }
`

export const DELETE_QUOTATION_DETAIL = gql`
  mutation DeleteQuotationDetail($id: ID!) {
    deletequotedetail(id: $id)
  }
`

export const DELETE_QUOTATION = gql`
  mutation DeleteQuotation($id: ID!) {
    deletequotation(id: $id)
  }
`

export const SET_QUOTATION_ACTIVE = gql`
  mutation SetActive($id: ID!, $CustomerCode: String) {
    setactive(id: $id, CustomerCode: $CustomerCode)
  }
`

export const NEW_QUOTATION = gql`
  mutation NewQuotation(
    $CustomerCode: String
    $CurrencyCode: String
    $AgreementDate: Date
    $EffectiveDate: Date
    $ExpiryDate: Date
    $Remark: String
    $AirlineCode: String
  ) {
    newquotation(
      CustomerCode: $CustomerCode
      CurrencyCode: $CurrencyCode
      AgreementDate: $AgreementDate
      EffectiveDate: $EffectiveDate
      ExpiryDate: $ExpiryDate
      Remark: $Remark
      AirlineCode: $AirlineCode
    ) {
      id
      QuotationNo
      CustomerCode
      CurrencyCode
      AgreementDate
      EffectiveDate
      ExpiryDate
      Remark
      Active
      AirlineCode
    }
  }
`

export const UPDATE_QUOTATION = gql`
  mutation UpdateQuotation(
    $id: ID!
    $CurrencyCode: String
    $AgreementDate: Date
    $EffectiveDate: Date
    $ExpiryDate: Date
    $Remark: String
    $AirlineCode: String
  ) {
    updatequotation(
      id: $id
      CurrencyCode: $CurrencyCode
      AgreementDate: $AgreementDate
      EffectiveDate: $EffectiveDate
      ExpiryDate: $ExpiryDate
      Remark: $Remark
      AirlineCode: $AirlineCode
    ) {
      id
      QuotationNo
      CustomerCode
      CurrencyCode
      AgreementDate
      EffectiveDate
      ExpiryDate
      Remark
      Active
      AirlineCode
    }
  }
`
