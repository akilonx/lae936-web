import React, { useState, useRef } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Grid,
  Card,
  CardContent,
  Typography,
  CardActionArea,
  CardActions,
  Button,
  CardMedia,
  List,
  Link,
} from '@material-ui/core/'

import Divider from '@material-ui/core/Divider'
import Container from '@material-ui/core/Container'
import { Link as Link2 } from 'react-router-dom'
import Hidden from '@material-ui/core/Hidden'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { useEffect } from 'react'
import ShopDetails from './shopDetails'
import Loading from '../common/loading'
import Menu from './menu'
import MenuCategory from './menuCategory'
import { GET_SHOPCONFIG } from '../graphql/config'
import { GET_CATEGORIES } from '../graphql/category'

const restApi = '/api'

const scrollToTop = () => {
  window.scrollTo(0, 0)
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  listRoot: {
    width: '100%',
    maxWidth: '36ch',
    padding: 0,
    overflowY: 'scroll',
    flexGrow: 1,
    backgroundColor: '#ebebeb',
  },
  media: {
    height: 150,
    margin: theme.spacing(1),
  },
  card: {},
  buttonRoot: {
    minWidth: 0,
    marginRight: 5,
    padding: 7,
  },
  dividerroot: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  containerroot: {
    paddingTop: 20,
    paddingBottom: 50,
    paddingLeft: 0,
    paddingRight: 0,
  },
}))

export default function Return(props) {
  const classes = useStyles()
  const [selectedProductID, setSelectedProductID] = useState()
  const [product, setProduct] = useState()
  const [back, setBack] = useState()
  const [backProductID, setBackProductID] = useState()
  const [currPage, setCurrPage] = useState(1)
  const [offset, setOffset] = useState(0)
  const [maxPages, updateMaxPages] = useState(0)
  const [viewLocation, setViewLocation] = useState()

  const [dbhost, setDbhost] = useState('spa1')

  /*const [dbhost, setDbhost] = useState('shop')*/

  const { data: { categories } = { categories: [] } } = useQuery(GET_CATEGORIES)

  const [totalAmount, setTotalAmount] = useState(0)

  const {
    loading: loadingConfig,
    data: { shopconfig } = { shopconfig: {} },
    refetch: refetchConfig,
  } = useQuery(GET_SHOPCONFIG)

  return (
    <React.Fragment>
      <Container classes={{ root: classes.containerroot }} maxWidth="md">
        <div className={classes.toolbar}>
          <div className={classes.root}>
            <h2>Terms & Condition</h2>
            <p>
              <strong>PAYMENT</strong>
            </p>
            <p>
              iPay88 Internet Payment Service is operated by Mobile88.com Sdn.
              Bhd. iPay88 and Mobile88.com will be shown on your Credit
              Card/Bank statement and you will also receive a notification
              e-mail from Mobile88 on any transaction.
            </p>
            <p>&nbsp;</p>
            <p>
              <strong>DELIVERY</strong>
            </p>
            <br />
            <p>
              <strong>We keep you informed</strong>
            </p>
            <p>
              Once you’ve placed an order, you will receive an Order
              Acknowledgement email to confirm your order details, including
              shipping and delivery estimates for each item.
            </p>
            <p>
              Once your item has prepared for shipment you will receive a
              Shipment Notification email with carrier and tracking information.
            </p>
            <p>
              You’ll see your order number, the date and time the order was
              placed, the status of the order, estimated shipping and delivery
              dates, the shipping method, and package tracking information for
              items that have shipped.
            </p>
            <p>
              The ‘Delivery’ timeframe is an estimate of when the item will be
              delivered to your shipping address after it ships.
            </p>

            <br />
            <p>
              <strong>When can I expect my order?</strong>
            </p>
            <p>
              Once your order has been handed over to the carrier for delivery,
              you will receive a shipment notification email with an estimated
              delivery date.
            </p>
            <p>
              Delivery will take 3 – 5 business days after the order has been
              shipped. All delivery lead times are estimated.
            </p>
            <p>
              We will be using Pos Laju Malaysia as the delivery service
              provider.
            </p>

            <br />
            <p>
              <strong>How can I track my order?</strong>
            </p>
            <p>
              As soon as your item(s) have shipped, we’ll send you a Shipment
              Notification email that includes the estimated delivery date and
              tracking number.
            </p>

            <br />
            <p>
              <strong>
                Can I have my order delivered to an address other than my home?
              </strong>
            </p>
            <p>
              For your convenience, you may prefer to have your order delivered
              to an alternative address such as your office address.
            </p>
            <p>
              If you choose to deliver to your office, please include the
              business name in the shipping address.
            </p>

            <br />
            <p>
              <strong>How much am I charged for shipping?</strong>
            </p>
            <p>
              It won’t cost you anything. Clarisound will be absorbing all
              shipping charges delivered within Malaysia.
            </p>

            <br />
            <p>
              <strong>REFUND POLICY</strong>
            </p>
            <p>No Refunds, Exchange only.</p>
            <p>
              You can return most new, unopened and unused items within 30 days
              of delivery for an exchange, (unfortunately, shipping charges are
              not refundable).
            </p>
            <p>
              Items must be returned in new, unopened and unused condition and
              contain all original materials included with the shipment.
            </p>
            <p>
              If the returned item is accepted after inspection by Clarisound,
              you will receive an exchange of the same product or a product of
              the same value.
            </p>
            <p>
              If the return is a result of our error, inform us immediately
              either via email or call us. We will ship the correct item (s) at
              no additional cost or shipping fee.
            </p>
            <p>
              How to return an item:
              <br />
              To return an item, you need to email us at{' '}
              <span style={{ color: '#ee5724' }}>customer@936lae.com</span> or
              call us at
              <br />
              <a href="tel:0000000000">03 5555 6666</a>
            </p>
            <br />
            <p>Office Hours:</p>
            <br />
            <p>
              PUCHONG OUTLET
              <br />
              J-7-1, Block J, SetiaWalk
              <br />
              Persiaran Wawasan
              <br />
              Pusat Bandar Puchong
              <br />
              47160 Puchong, Selangor Darul Ehsan.
              <br />
              Tel: +603 – 5882 0096
              <br />
              Operation hours: 10:00am – 6:00pm, Tues – Sat
            </p>
            <p>
              Form of payment.
              <br />
              Non-Returnable Items
            </p>
            <p>
              Items that are opened, in used condition, or beyond 30 days of
              delivery may not be returned or exchanged.
            </p>
            <p>We apologize for any inconvenience this may cause</p>
            <br />
            <p>
              Damaged Shipment
              <br />
              If the carton arrived damaged, please contact us
            </p>
            <p>
              Tel: +603 – 5882 0096
              <br />
              Operation hours: 10.00am – 6.00pm, Tues – Sat
              <br />
              Or email us customer@clarisound.com
            </p>
            <p>
              If a damaged item is returned, we will ship an undamaged item at
              no additional cost of shipping fee.
            </p>
            <br />
            <p>
              Defective Goods:
              <br />
              If the product(s) arrived in non-working order, please contact us
              at
              <br />
              contact us Tel: +603 – 5882 0096
              <br />
              Operation hours: 10:00am – 6:00pm, Tues – Sat
              <br />
              Or email us customer@clarisound.com
            </p>
            <br />
            <p>
              Exchange Policy:
              <br />
              After Clarisound receives your item, it will be inspected. If the
              item is returned in new, unopened, and unused condition with the
              and within 30 days of delivery, we will exchange the product.
            </p>
          </div>
        </div>
      </Container>
    </React.Fragment>
  )
}
